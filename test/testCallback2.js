const path = require("path");
const boardData = require(path.join(__dirname, "./data/boards.json"));
const listData = require(path.join(__dirname, "./data/lists.json"));

const getListInformations = require(path.join(__dirname, "../callback2"));
let boardId = boardData[Math.floor(Math.random() * boardData.length)].id;

const callback = (err, data) => {
  if (data) {
    console.log(data);
  } else {
    console.log(err.message);
  }
};
getListInformations(boardId, listData, callback);
