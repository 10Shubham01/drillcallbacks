const path = require("path");
const boardData = require(path.join(__dirname, "./data/boards.json"));
const listData = require(path.join(__dirname, "./data/lists.json"));
const cardData = require(path.join(__dirname, "./data/cards.json"));
const getInformations = require(path.join(__dirname, "../callback6"));

getInformations("Thanos", boardData, listData, cardData);
