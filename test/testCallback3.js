const path = require("path");
const boardData = require(path.join(__dirname, "./data/boards.json"));
const listData = require(path.join(__dirname, "./data/lists.json"));
const cardData = require(path.join(__dirname, "./data/cards.json"));

const getCardsInformations = require(path.join(__dirname, "../callback3"));
let boardId = boardData[Math.floor(Math.random() * boardData.length)].id;
let listId =
  listData[boardId][Math.floor(Math.random() * listData[boardId].length)].id;

const callback = (err, data) => {
  if (data) {
    console.log(data);
  } else {
    console.log(err.message);
  }
};
getCardsInformations(listId, cardData, callback);
