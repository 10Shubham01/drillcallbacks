const getBoardInformations = (boardId, boardData, callback) => {
  setTimeout((data) => {
    if (
      boardId == null ||
      Array.isArray(boardData) == false ||
      typeof callback != "function"
    ) {
      console.log("Inappropriate data has been passed");
    } else {
      data = boardData.filter((element) => element.id === boardId);
      if (data) {
        callback(null, data);
      } else {
        let err = new Error("Data n ot found");
        callback(err);
      }
    }
  }, 2000);
};
module.exports = getBoardInformations;
