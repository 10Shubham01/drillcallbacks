const path = require("path");
const getBoardInformations = require(path.join(__dirname, "./callback1"));
const getListInformations = require(path.join(__dirname, "./callback2"));
const getCardInformations = require(path.join(__dirname, "./callback3"));

const getInformations = (
  boardName,
  listName,
  boardData,
  listData,
  cardData
) => {
  setTimeout(() => {
    if (
      boardName == null ||
      Array.isArray(listName) == false ||
      Array.isArray(boardData) == false ||
      typeof listData != "object" ||
      typeof cardData != "object"
    ) {
      console.log("Inappropriate data has been passed");
    } else {
      boardData.filter((element) => {
        if (element.name === boardName) {
          getBoardInformations(element.id, boardData, (err, data) => {
            if (data) {
              console.log(data);
            } else {
              console.log(err.message);
            }
            getListInformations(element.id, listData, (err, data) => {
              if (data) {
                console.log(data);
              } else {
                console.log(err.message);
              }
              data.filter((element) => {
                listName.forEach((e) => {
                  if (element.name === e) {
                    getCardInformations(element.id, cardData, (err, data) => {
                      if (data) {
                        console.log(data);
                      } else {
                        console.log(err.message);
                      }
                    });
                  }
                });
              });
            });
          });
        }
      });
    }
  }, 2 * 1000);
};
module.exports = getInformations;
