const getListInformations = (boardId, listData, callback) => {
  setTimeout((data) => {
    if (
      boardId == null ||
      typeof listData != "object" ||
      typeof callback != "function"
    ) {
      console.log("Inappropriate data has been passed");
    } else {
      data = listData[boardId];
      if (data) {
        callback(null, data);
      } else {
        let err = new Error("Data not found");
        callback(err);
      }
    }
  }, 2000);
};
module.exports = getListInformations;
