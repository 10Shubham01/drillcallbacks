const getCardInformations = (listId, cardData, callback) => {
  setTimeout((data) => {
    if (
      listId == null ||
      typeof cardData != "object" ||
      typeof callback != "function"
    ) {
      console.log("Inappropriate data has been passed");
    } else {
      data = cardData[listId];
      if (data) {
        callback(null, data);
      } else {
        let err = new Error("Data not found");
        callback(err);
      }
    }
  }, 2000);
};
module.exports = getCardInformations;
